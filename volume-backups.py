import boto3
import schedule
import time

ec2_client = boto3.client('ec2', region_name="eu-west-2")

def create_volume_snapshots():
    volumes = ec2_client.describe_volumes()
    for volume in volumes['Volumes']:
        new_snapshot = ec2_client.create_snapshot(
            VolumeId=volume['VolumeId']
        )
        print(new_snapshot)

        # Introduce a sleep interval to avoid exceeding the rate limit
        time.sleep(10)  # Adjust the sleep interval as needed

schedule.every(5).seconds.do(create_volume_snapshots)

while True:
    schedule.run_pending()

