# Backup EC2 Instance Volumes with Python

#### Project Outline
In this project, we will create snapshots of Volumes for EC2 instances, where the Volumes are AWS storage components for EC2 instance data. Volume snapshots are a copy of the volume. We can automate it using python.

Lets imagine a scenario where we have multiple ec2 instances and they crash and they lose their volume as well, we can implement a mechanism to take daily snapshots of the volume and restore the ec2 volumes from it and we can automate it instead of taking snapshots manually.

#### Lets get started

Lets create two ec2 instances one for prod and one for dev

![Image1](https://gitlab.com/FM1995/backup-ec2-instance-volumes-with-python/-/raw/main/Images/Image1.png)

Can see the volumes getting created

![Image2](https://gitlab.com/FM1995/backup-ec2-instance-volumes-with-python/-/raw/main/Images/Image2.png)

Can also see the snapshots empty

![Image3](https://gitlab.com/FM1995/backup-ec2-instance-volumes-with-python/-/raw/main/Images/Image3.png)

Now lets create the python script

Can then using the below documentation proceed further

https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ec2/client/describe_volumes.html

Can start off by using the client just like the below

![Image4](https://gitlab.com/FM1995/backup-ec2-instance-volumes-with-python/-/raw/main/Images/Image4.png)

Since it is a dictionary response, we need to get the list

![Image5](https://gitlab.com/FM1995/backup-ec2-instance-volumes-with-python/-/raw/main/Images/Image5.png)

Now to create a snapshot of the ec2 instances

Using the below documentation, can create a snapshot

https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ec2/client/create_snapshot.html

Can then loop it and configure the below

![Image6](https://gitlab.com/FM1995/backup-ec2-instance-volumes-with-python/-/raw/main/Images/Image6.png)

When executed it then returns the below

![Image7](https://gitlab.com/FM1995/backup-ec2-instance-volumes-with-python/-/raw/main/Images/Image7.png)

Can see the newly created snapshots

![Image8](https://gitlab.com/FM1995/backup-ec2-instance-volumes-with-python/-/raw/main/Images/Image8.png)

Now we can implement scheduling to take frequent snapshots


Can then import schedule and configure the scheduler

![Image9](https://gitlab.com/FM1995/backup-ec2-instance-volumes-with-python/-/raw/main/Images/Image9.png)

Can then configure the scheduler to run by setting it to a function

Adding sleep interval

![Image10](https://gitlab.com/FM1995/backup-ec2-instance-volumes-with-python/-/raw/main/Images/Image10.png)

Deleting the snapshots and checking again and can see they have been created

![Image11](https://gitlab.com/FM1995/backup-ec2-instance-volumes-with-python/-/raw/main/Images/Image11.png)

Finalised for ec2 backup volumes

![Image12](https://gitlab.com/FM1995/backup-ec2-instance-volumes-with-python/-/raw/main/Images/Image12.png)

Now lets say we want to backup only Production tagged EC2-Instances

In describe volumes function can filter

Just like the below

![Image13](https://gitlab.com/FM1995/backup-ec2-instance-volumes-with-python/-/raw/main/Images/Image13.png)

For simplicity can add the tags to the volume manually

![Image14](https://gitlab.com/FM1995/backup-ec2-instance-volumes-with-python/-/raw/main/Images/Image14.png)

Can now implement it like the below


![Image15](https://gitlab.com/FM1995/backup-ec2-instance-volumes-with-python/-/raw/main/Images/Image15.png)

Can then do it like the below where it will do 1 snapshot every 20 seconds for the prod ec2 volume

![Image16](https://gitlab.com/FM1995/backup-ec2-instance-volumes-with-python/-/raw/main/Images/Image16.png)

Can see the snapshots getting created

![Image17](https://gitlab.com/FM1995/backup-ec2-instance-volumes-with-python/-/raw/main/Images/Image17.png)






